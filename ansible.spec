%global with_docs 1
Name: ansible
Summary: SSH-based configuration management, deployment, and task execution system
Version: 2.9.27
Release: 7

License: GPLv3+
Source0: https://releases.ansible.com/ansible/%{name}-%{version}.tar.gz
Source1: ansible.attr
Source2: ansible-generator
Source3: macros.ansible
Url: http://ansible.com
BuildArch: noarch

Patch1: ansible-2.9.22-rocky.patch
Patch2: ansible-2.9.6-disable-test_build_requirement_from_path_no_version.patch
Patch3: fix-python-3.9-compatibility.patch
Patch4: ansible-2.9.23-sphinx4.patch
Patch5: hostname-module-support-openEuler.patch
Patch6: Fix-build-error-for-sphinx-7.0.patch
Patch7: CVE-2024-0690.patch
Patch8: CVE-2024-8775.patch
Patch9: CVE-2024-9902.patch
Patch10:CVE-2022-3697.patch
Patch11:CVE-2023-5115.patch

Provides:      ansible-python3 = %{version}-%{release}
Obsoletes:     ansible-python3 < %{version}-%{release}
Conflicts: ansible-base > 2.10.0
Conflicts: ansible-core > 2.11.0

BuildRequires: python3-packaging
BuildRequires: python3-pexpect
BuildRequires: python3-paramiko
BuildRequires: python3-pywinrm
BuildRequires: python3-pbkdf2
BuildRequires: python3-httmock
BuildRequires: python3-python-gitlab
BuildRequires: python3-boto3
BuildRequires: python3-botocore
BuildRequires: python3-coverage
BuildRequires: python3-passlib
BuildRequires: git-core
BuildRequires: openssl
%if 0%{?with_docs}
BuildRequires: python3-sphinx
BuildRequires: python3-sphinx-theme-alabaster
BuildRequires: python3-sphinx-notfound-page
BuildRequires: asciidoc
BuildRequires: python3-straight-plugin
%endif
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-six
BuildRequires: python3-pytest
BuildRequires: python3-pytest-xdist
BuildRequires: python3-pytest-mock
BuildRequires: python3-requests
BuildRequires: python3-mock
BuildRequires: python3-jinja2
BuildRequires: python3-pyyaml
BuildRequires: python3-cryptography
BuildRequires: python3-pyvmomi
BuildRequires: make

Recommends: python3-paramiko
Recommends: python3-winrm
Requires: python3-setuptools
Requires: python3-six
Requires: python3-jinja2
Requires: python3-pyyaml
Requires: sshpass
Requires: python3-jmespath

%description
Ansible is a radically simple model-driven configuration management,
multi-node deployment, and remote task execution system. Ansible works
over SSH and does not require any software or daemons to be installed
on remote nodes. Extension modules can be written in any language and
are transferred to managed machines automatically.

%package -n ansible-doc
Summary: Documentation for Ansible

%description -n ansible-doc

Ansible is a radically simple model-driven configuration management,
multi-node deployment, and remote task execution system. Ansible works
over SSH and does not require any software or daemons to be installed
on remote nodes. Extension modules can be written in any language and
are transferred to managed machines automatically.

This package installs extensive documentation for ansible

%package -n ansible-test
Summary: Tool for testing ansible plugin and module code
Requires: %{name} = %{version}-%{release}

%description -n ansible-test
Ansible is a radically simple model-driven configuration management,
multi-node deployment, and remote task execution system. Ansible works
over SSH and does not require any software or daemons to be installed
on remote nodes. Extension modules can be written in any language and
are transferred to managed machines automatically.

This package installs the ansible-test command for testing modules and plugins
developed for ansible.


%prep
%autosetup -p1
cp -a %{S:1} %{S:2} %{S:3} .

%build
sed -i -e 's|/usr/bin/env python|/usr/bin/python3|' test/lib/ansible_test/_data/*.py test/lib/ansible_test/_data/*/*.py test/lib/ansible_test/_data/*/*/*.py docs/bin/find-plugin-refs.py

sed -i -s 's|/usr/bin/env pwsh||' test/lib/ansible_test/_data/sanity/validate-modules/validate_modules/ps_argspec.ps1
sed -i -s 's|/usr/bin/env pwsh||' test/lib/ansible_test/_data/sanity/pslint/pslint.ps1
sed -i -s 's|/usr/bin/env pwsh||' test/lib/ansible_test/_data/requirements/sanity.ps1

%global py3_shbang_opts %(echo %{py3_shbang_opts} | sed 's/-s//')
%py3_build

%if 0%{?with_docs}
  make PYTHON=/usr/bin/python3 SPHINXBUILD=sphinx-build-3 webdocs
%else
  make PYTHON=/usr/bin/python3 -Cdocs/docsite config cli keywords modules plugins testing
%endif

%install
%py3_install

DATADIR_LOCATIONS='%{_datadir}/ansible/collections
%{_datadir}/ansible/collections/ansible_collections
%{_datadir}/ansible/plugins/doc_fragments
%{_datadir}/ansible/plugins/action
%{_datadir}/ansible/plugins/become
%{_datadir}/ansible/plugins/cache
%{_datadir}/ansible/plugins/callback
%{_datadir}/ansible/plugins/cliconf
%{_datadir}/ansible/plugins/connection
%{_datadir}/ansible/plugins/filter
%{_datadir}/ansible/plugins/httpapi
%{_datadir}/ansible/plugins/inventory
%{_datadir}/ansible/plugins/lookup
%{_datadir}/ansible/plugins/modules
%{_datadir}/ansible/plugins/module_utils
%{_datadir}/ansible/plugins/netconf
%{_datadir}/ansible/roles
%{_datadir}/ansible/plugins/strategy
%{_datadir}/ansible/plugins/terminal
%{_datadir}/ansible/plugins/test
%{_datadir}/ansible/plugins/vars'

UPSTREAM_DATADIR_LOCATIONS=$(grep -ri default lib/ansible/config/base.yml| tr ':' '\n' | grep '/usr/share/ansible')

if [ "$SYSTEM_LOCATIONS" != "$UPSTREAM_SYSTEM_LOCATIONS" ] ; then
	echo "The upstream Ansible datadir locations have changed.  Spec file needs to be updated"
	exit 1
fi

mkdir -p $RPM_BUILD_ROOT%{_datadir}/ansible/plugins/
for location in $DATADIR_LOCATIONS ; do
	mkdir $RPM_BUILD_ROOT"$location"
done
mkdir -p $RPM_BUILD_ROOT/etc/ansible/
mkdir -p $RPM_BUILD_ROOT/etc/ansible/roles/

cp examples/hosts $RPM_BUILD_ROOT/etc/ansible/
cp examples/ansible.cfg $RPM_BUILD_ROOT/etc/ansible/
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
cp -v docs/man/man1/*.1 $RPM_BUILD_ROOT/%{_mandir}/man1/

cp -pr docs/docsite/rst .
%if 0%{?with_docs}
  cp -pr docs/docsite/_build/html %{_builddir}/%{name}-%{version}/html
%endif

install -Dpm0644 -t %{buildroot}%{_fileattrsdir} ansible.attr
install -Dpm0644 -t %{buildroot}%{_rpmmacrodir} macros.ansible
install -Dpm0755 -t %{buildroot}%{_rpmconfigdir} ansible-generator


%check
%if 0%{?with_tests}
ln -s /usr/bin/pytest-3 bin/pytest
pathfix.py -i %{__python3} -p test/lib/ansible_test/_data/cli/ansible_test_cli_stub.py
rm -f test/units/modules/cloud/cloudstack/test_cs_traffic_type.py
rm -f test/units/module_utils/facts/hardware/test_sunos_get_uptime_facts.py
rm -f test/units/modules/source_control/test_gitlab_runner.py
rm -f test/units/plugins/lookup/test_aws_secret.py
rm -f test/units/plugins/lookup/test_aws_ssm.py
make PYTHON=/usr/bin/python3 tests-py3
%endif

%files
%license COPYING
%doc README.rst PKG-INFO changelogs/CHANGELOG-v2.9.rst
%doc %{_mandir}/man1/ansible*
%config(noreplace) %{_sysconfdir}/ansible/
%{_bindir}/ansible*
%{_datadir}/ansible/
%{python3_sitelib}/ansible
%{python3_sitelib}/ansible_test
%{python3_sitelib}/*egg-info
%{_fileattrsdir}/ansible.attr
%{_rpmmacrodir}/macros.ansible
%{_rpmconfigdir}/ansible-generator
%exclude %{_bindir}/ansible-test
%exclude %{python3_sitelib}/ansible_test

%files -n ansible-doc
%doc rst
%if 0%{?with_docs}
%doc html
%endif

%files -n ansible-test
%{_bindir}/ansible-test
%{python3_sitelib}/ansible_test

%changelog
* Sat Feb 08 2025 wangkai <13474090681@163.com> - 2.9.27-7
- Fix CVE-2022-3697 CVE-2023-5115

* Mon Dec 02 2024 yaoxin <yao_xin001@hoperun.com> - 2.9.27-6
- Fix CVE-2024-8775 and CVE-2024-9902

* Mon Feb 05 2024 wangkai <13474090681@163.com> - 2.9.27-5
- Fix CVE-2024-0690

* Wed Aug 9 2023 liyanan <thistleslyn@163.com> - 2.9.27-4
- Remove obsolete buildrequire python3-crypto

* Thu Aug 3 2023 liyanan <thistleslyn@163.com> - 2.9.27-3
- Fix build error for sphinx 7.0

* Fri Apr 29 2022 wangkai <wangkai385@h-partners.com> - 2.9.27-2
- Hostname module support openEuler and remove python-nose

* Tue Apr 26 2022 yangping <yangping69@h-partners.com> - 2.9.27-1
- update to 2.9.27

* Thu Oct 28 2021 liwu <liwu13@huawei.com> - 2.9.24-4
- The upstream community rolls back the patch
 
* Fri Sep 17 2021 liwu <liwu13@huawei.com> - 2.9.24-3
- fix CVE-2020-1738

* Thu Sep 16 2021 liwu <liwu13@huawei.com> - 2.9.24-2
- fix CVE-2020-1736

* Tue Aug 17 2021 wulei <wulei80@huawei.com> - 2.9.24-1
- update to 2.9.24

* Mon Jul 12 2021 liksh <liks11@chinaunicom.cn> - 2.9.0-1
- update to 2.9.0

* Tue Jan 12 2021 yanan li <liyanan32@huawei.com> - 2.5.5-1
- Package init
